# Groovy Invaders!

![Groovy Invaders header](readme/banner.png)

This is a simple little game that I'm using to learn a bit about using groovy and JavaFX. Your objective is to defeat the incoming aliens from destroying you. There are 5 levels, each one harder than the last!

## How to run

I spent 3-4 days trying to get a .jar file running that I could share but couldn't manage to get it done. So if you want to play the game, copy the source code and run the command `gradlew run` from your terminal. I know this isn't very portable, but it is what it is.

Make sure you have Java installed first.

## Controls

**Start new game:** form title/end screen, press ENTER  
**Move:** Left/right arrow keys  
**Shoot** Automatic

<img src="readme/screenshot-1.png" alt="Screenshot-1" width="45%"/> <img src="readme/screenshot-2.png" alt="Screenshot-2" width="45%"/> 

## Resources credits

* Groovy sfx: Evil Dead 2
* Robot voice: https://lingojam.com/RobotVoiceGenerator
* Music: Context Sensitive
* Explosion sfx: Effects Grinder
* Explosion animation: BlackDragon1727
* Laser sfx: Pixabay